<?php
    header("Content-Type: application/json; charset=UTF-8");
    header('Access-Control-Allow-Origin: *'); // RESOLVE CORS ERROR :) :D 
    
    include 'shared/require.php';

    $a = new Attendee(25, 003, 02, 'http;//www.gitlab.it');
    $b = new Attendee(68, 7845, 01, 'http://www.github.io');
    $c = new Attendee(41, 7845, 01, 'http://www.worker.io');
    $tab = [$a, $b, $c];
    
    $response = new stdClass();
    $response->message = "Attendees in JSON";
    $response->data = $tab;    
    
    echo json_encode($response);
