<?php
    header("Content-Type: application/json; charset=UTF-8");
    header('Access-Control-Allow-Origin: *'); // RESOLVE CORS ERROR :) :D 
    
    include 'shared/require.php';

    $a = new Event(01, 'Google IO', 1, 'davidhihea@gmail.com');
    $b = new Event(02, 'Java Bootcamp', 45896, 'youdavidem@hotmail.com');
    $c = new Event(03, 'Yello Summer', 1, 'davidhihea@gmail.com');
    $tab = [$a, $b, $c];
    
    $response = new stdClass();
    $response->message = "Events in JSON";
    $response->data = $tab;    
    
    echo json_encode($response);
