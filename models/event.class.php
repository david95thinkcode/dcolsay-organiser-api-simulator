<?php 

    class Event {
        public $id;
        public $title;
        public $description;
        public $organiser_id;
        public $organiser_email;

        public function __construct($id, $title, $organiser_id, $organiser_mail) {
            $this->id = $id;
            $this->title = $title;
            $this->organiser_id = $organiser_id;
            $this->organiser_email = $organiser_mail;
        }

    }
