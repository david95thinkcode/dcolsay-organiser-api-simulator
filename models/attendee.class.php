<?php 

class Attendee {
    public $id;
    public $owner_id;
    public $event_id;
    public $barcode;    

    public function __construct($id, $owner_id, $event_id, $barcode) {
        $this->id = $id;
        $this->owner_id = $owner_id;
        $this->event_id = $event_id;
        $this->barcode = $barcode;
    }

    public function toJSON() {
        // return json_encode($this);
    }
}